<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Editor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    /**
     * @Route("/", name="base")
     */
    public function base()
    {
        $repo = $this->getDoctrine()->getRepository(Book::class);
        $books = $repo->findAll();
        return $this->render('base/index.html.twig', [
            'books' => $books,
        ]);
    }

    /**
     * @Route("/book", name="book")
     */
    public function book()
    {
        $repo = $this->getDoctrine()->getRepository(Book::class);
        $books = $repo->findAll();
        return $this->render('base/book.html.twig', [
            'books' => $books,
        ]);
    }

    /**
     * @Route("/editor", name="editor")
     */
    public function editor()
    {
        $repo = $this->getDoctrine()->getRepository(Editor::class);
        $editors = $repo->findAll();
        return $this->render('base/editor.html.twig', [
            'editors' => $editors,
        ]);
    }

    /**
     * @Route("/author", name="author")
     */

    public function Author()
    {
        return $this->render('base/author.html.twig', [
            'controller_name' => 'BaseController',
        ]);
    }
}
