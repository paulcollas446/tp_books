<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    protected $em;

    // on instancie une instance de manager pour palier le soucis d'auto-wiring
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/registration", name="security_registration")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $encoder)
    {
        // un nouvel utilisateur
        $user = new User();
        // qui va completer le form si le user contient des infos
        $form = $this->createForm(RegistrationType::class, $user);
        // le formulaire va gerer la requete HTTP (GET/POST)
        $form->handleRequest($request);
        // si requete de type POST et que les champs sont valides
        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());
            // $hash = $argon2i$v=19$m=65536,t=4,p=1$TDh1Ny51MEdpS01CaWI0cA$m2rRwseLtwCuHsPa7TK4Epa8zkrQk7tp1eRcBXSno88
            $user->setPassword($hash);

            // on donne un role utilisateur à notre user
            $user->setRoles(['ROLE_USER']);

            $this->em->persist($user);
            $this->em->flush();
        }

        return $this->render('security/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

        /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $au)
    {
        $error = $au->getLastAuthenticationError();
        $username = $au->getLastUsername();
        return $this->render('security/login.html.twig', [
            'lastUserName' => $username, 
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout()
    {
        
    }
}


